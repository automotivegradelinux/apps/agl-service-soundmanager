#!/bin/sh

echo "please call method after launch afb-client-demo"
echo "  # ./sm-binding-test.sh"
echo "To get soundmanager api "
echo "example) monitor get {\"apis\":[\"monitor\"]}"
echo "example) soundmanager disconnect {\"mainConnectionID\":1}"
port=12345
token=123456
if test $1;then
port=$1
fi
if test $2; then
token=$2
fi
afb-client-demo ws://localhost:${port}/api?token=${token}

