#!/bin/sh

echo "launch soundmanager binder test"
cmd=/usr/bin/afb-daemon
libsm="soundmanager-service.so"
port="12345"
token="123456"
if test $1; then
 port=$1
fi
if test $2; then
 token=$2
fi
libpath="/home/root"
arg="--verbose --verbose --verbose --port=${port} --token=${token} --binding=${libpath}/${libsm}"
echo "gdb $cmd $arg"
gdb $cmd
echo $! $?
if(test $! -gt 0); then
 echo "success to launch"
fi
