/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AM_HELPER_H
#define AM_HELPER_H
#define _GNU_SOURCE
#define AFB_BINDING_VERSION 2
#include <afb/afb-binding.h>
#include <stdint.h>
#include <stdbool.h>
#include <glib.h>
#include "sm-def.h"
#define SEND_RESULT(...) send_result(__VA_ARGS__, __FUNCTION__)
#define SEND_RESULT_NO_RESP(...) send_result_no_resp(__VA_ARGS__, __FUNCTION__)

static const char* cmd_evlist[] = {
    SM_EVENT_VOLUME_CHANGED,
    SM_EVENT_NEW_MAIN_CONNECTION,
    SM_EVENT_REMOVED_MAIN_CONNECTION,
    SM_EVENT_SINK_MUTE_STATE_CHANGED,
    SM_EVENT_MAIN_CONNECTION_STATE_CHANGED
};

static const char* route_evlist[] = {
    /* Routing event*/
    SM_EVENT_SET_ROUTING_READY,
    SM_EVENT_SET_ROUTING_RUNDOWN,
    SM_EVENT_ASYNC_CONNECT,
    SM_EVENT_ASYNC_SET_SOURCE_STATE,
    SM_EVENT_ASYNC_DISCONNECT,
    SM_EVENT_STREAM_STATE_EVENT
};

REQ_ERROR get_value_uint16(const struct afb_req request, const char *source, uint16_t *out_id);
REQ_ERROR get_value_int16(const struct afb_req request, const char *source, int16_t *out_id);
REQ_ERROR get_value_int32(const struct afb_req request, const char *source, int32_t *out_id);
REQ_ERROR get_sink_id(const struct afb_req request, const char* key, uint16_t *out_sink_id);
void set_default_sinkID(int sinkID);
void sm_add_object_to_json_object(struct json_object* j_obj, int count, ...);
void sm_add_object_to_json_object_func(struct json_object* j_obj, const char* verb_name, int count, ...);
int sm_search_event_name_index(const char* value);
int sm_search_routing_event_name_index(const char* value);
bool send_result_no_resp(ErrorCode ec, const char* function);
bool send_result(ErrorCode ec, struct afb_req req, const char* function);
ErrorCode is_range_over_guint16(int source);
ErrorCode is_range_over_gint16(int source);
#endif /*AM_HELPER_H*/