/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#define _GNU_SOURCE
#define AFB_BINDING_VERSION 2
#include <afb/afb-binding.h>

/*
********** Method of Sound Manager (API) **********
*/

/**
 * Call "connect" function of Audio Manager.
 * Getting resource right to output audio stream.
 * Please input following keys with value into json string object in argument.
 * In case of using libsoundmanager,
 * json_object *jobj = json_object_new_object();
 * json_object_object_add(jobj, "sourceID", json_object_new_int(100));
 * json_object_object_add(jobj, "sinkID", json_object_new_int(100));
 * call("connect", jobj);
 *
 * The order of arguments is not important.
 *
 * #### Parameters
 * Request key
 * - sourceID   : Source ID getting in return value in registerSource or appname.
 * - sinkID     : Sink ID. This should be more than 0. If no request, defalut value is set by sound manager
 *
 * This will be changed in near future because these arguments should be aliased like
 * sinkID:100 -> sinkID:"speaker"
 *
 * This will be modified after integrating
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 * sourceID and sinkID should be more than 0
 *
 */
void connect (struct afb_req request);

/**
 * Call "disconnect" function of Audio Manager.
 * Release resource right to output audio stream.
 *
 * #### Parameters
 * Request key
 * - sourceID   : Source ID getting in return value in registerSource or appname.
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 * sourceID should be more than 0
 *
 */
void disconnect (struct afb_req request);

/**
 * Call "setVolume" function of Audio Manager.
 * Set sink volume.
 *
 * #### Parameters
 * Request key
 * - volume   : volume value. The range of value should be [0-100]
 * - sinkID   : sinkID you would like to change volume at
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void setVolume (struct afb_req request);

/**
 * Call "volumeStep" function of Audio Manager.
 * Change volume step of sink
 *
 * #### Parameters
 * Request key
 * - sinkID     : sinkID you would to change volume step
 * - volumeStep : Step size of volume
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void volumeStep (struct afb_req request);

/**
 * Call "volumeStep" function of Audio Manager.
 * Change volume step of sink
 *
 * #### Parameters
 * Request key
 * - sinkID     : sinkID you would like to change mute state
 * - muteState  : muteState, 1 means mute, 2 means unmute. Or you can designate as "mute" or "unmute"
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void setSinkMuteState(struct afb_req request);

/**
 * Call "getListMainConnections" function of Audio Manager.
 * Get mainc connection list
 *
 * #### Parameters
 * Request key
 * None
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message. Even if there is no connection list,
 *   Sound Manager return success.
 *
 * #### Note
 *
 */
void getListMainConnections(struct afb_req request);

/**
 * Call "getListMainSources" function of Audio Manager.
 * Get main source list
 *
 * #### Parameters
 * Request key
 * None
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message. Even if there is no connection list,
 *   Sound Manager return success.
 *
 * #### Note
 *
 */
void getListMainSources(struct afb_req request);

/**
 * Call "getListMainSinks" function of Audio Manager.
 * Get main sink list
 *
 * #### Parameters
 * Request key
 * None
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message. Even if there is no connection list,
 *   Sound Manager return success.
 *
 * #### Note
 *
 */
void getListMainSinks(struct afb_req request);

/**
 * Call "ackConnect" function of Audio Manager.
 * Return acknowledge of connect against asyncConnect
 *
 * #### Parameters
 *  - handle  : Handle id when you get on asyncConnect
 *  - connectionID : connection id when you got on connect return value
 *  - error   : Error Number you would like to send. If error is 0, it means OK.
 *              If an application has some error, send error number in function then AM release
 *              resources the application got in connect.
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message. Even if there is no connection list,
 *   Sound Manager return success. So you should check the contents size of return json object
 *
 * #### Note
 *
 */
void ackConnect(struct afb_req request);

/**
 * Call "ackDisconnect" function of Audio Manager.
 * Return acknowledge of disconnect against asyncDisconnect
 *
 * #### Parameters
 *  - handle  : Handle id when you get on asyncDisconnect
 *  - connectionID : connection id when you got on connect return value
 *  - error   : Error Number you would like to send. If error is 0, it means OK.
 *              If an application has some error, send error number in function then AM
 *
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message. Even if there is no connection list,
 *   Sound Manager return success. So you should check the contents size of return json object
 *
 * #### Note
 *
 */
void ackDisconnect(struct afb_req request);

/**
 * Call "ackSetSourceState" function of Audio Manager.
 * Return acknowledge of setSourceState against asyncSetSourceState.
 *
 * #### Parameters
 *  - handle  : Handle id when you get on asyncSetSourceState
 *  - error   : Error Number you would like to send. If error is 0, it means OK.
 *              If an application has some errors, send error number in function
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 * This function is very important for applications to realise the sequence of Audio Management.
 * An Application which matches with sourceID in the parameter of asyncSetSourceState has to return ack to use this function
 *
 */
void ackSetSourceState(struct afb_req request);

/**
 * Call "registerSource" function of Audio Manager.
 * Register source(application) to Audio Manager Policy Management
 * Application must call this function on its initialization
 *
 * #### Parameters
 *  - appname  : Application unieque name
 * [Option]
 * It is not necessary to designate following argument, because these are default value is selected y soundmanager
 * If you would like to set value, please input the following key and value
 *   - sourceClassID :
 *   - sourceState   :
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void registerSource(struct afb_req request);

/**
 * Call "deregisterSource" function of Audio Manager.
 * Deregister source(application) to Audio Manager Policy Management
 *
 * #### Parameters
 *  - sourceID  : sourceID returned in resisterSource
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void deregisterSource(struct afb_req request);

/**
 * Subscribe event
 *
 * #### Parameters
 *  - event  : Event name. Event list is written in libsoundmanager.hpp
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void subscribe(struct afb_req request);

/**
 * Unsubscribe event
 *
 * #### Parameters
 *  - event  : Event name. Event list is written in libsoundmanager.hpp
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 *
 * #### Note
 *
 */
void unsubscribe(struct afb_req request);

#ifdef ENABLE_AGL_AHL

/**
 * Application High Level API for AGL
 * This function opens "stream".
 * "stream" means the routing from source to sink.
 * This function calls registerSource and register the endpoint.
 * If the endpoint is not registered in AudioManager, Sound Manager uses default value.
 * audio_role will be translated to "sourceID", and "endpoint_id" will be translated to "sinkID".
 *
 * #### Parameters
 *  - audio_role  :   audio role such like entertainment, emergency.
 *                    This name is depends on the system architect.
 *                    The name will be application name or will be the group(role) of application.
 *  - endpoint_type:  The type of endpoint which means source/sink. As default, Sound Manager uses it as source.
 *                    sink is not supported for now.
 *  - endpoint_id :   Same as sinkID in Sound Manager.
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 * - stream_id  : Same as sourceID but key is different. This is for AHL.
 * #### Note
 * TODO : write note
 */
void stream_open(struct afb_req req);

/**
 * Application High Level API for AGL
 * This function closes "stream".
 * This function calls disconnect with registered connectionID translated with requested connection_id.
 * If the connection is not created in Sound Manager, Sound Manager just returns with error message.
 *
 * #### Parameters
 *  - stream_id : Stream id which is returned in stream_open.
 *
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 * #### Note
 * TODO : write note
 */
void stream_close(struct afb_req req);

/**
 * Application High Level API for AGL
 * This function set stream state.
 * This function set the availability and calls connect function of Audio Manager.
 * Policy check result will be notified by event..
 *
 * #### Parameters
 *  - stream_id : Stream id which is returned in stream_open.
 *  - mute      : Stream state as int. 0 is unmute, and 1 is mute. Sound manager set this parameter as unmute as default
 * #### Return
 * - error      : Error status number. If error is 0, it means the request is accepted, otherwise error message is attached with error code in reply message.
 * #### Note
 * After set unmute, then 'asyncSetSourceState' and 'stream_state_event' is pushed. These events contains information of state judged by AudioManager.
 */
void set_stream_state(struct afb_req req);

/*
********** Event list from Sound Manager **********
*/

#endif


/*
********** Application Framework Imprement **********
*/

/*
 * array of the verbs exported to afb-daemon
 */
const struct afb_verb_v2 binding_verbs[]= {
#ifdef ENABLE_AGL_AHL
// High Level API of AGL
{ .verb = "stream_open",            .callback = stream_open,            .auth = NULL,
                .info = "Open stream." ,       .session = AFB_SESSION_NONE},
{ .verb = "stream_close",           .callback = stream_close,           .auth = NULL,
                .info = "Close stream" ,       .session = AFB_SESSION_NONE},
{ .verb = "set_stream_state",       .callback = set_stream_state,       .auth = NULL,
                .info = "Set stream state" ,   .session = AFB_SESSION_NONE},
#endif
// Adaption API of Audio Manager
{ .verb = "connect",                .callback = connect,                .auth = NULL,
                .info = "Connect source id and sink id" ,       .session = AFB_SESSION_NONE},
{ .verb = "disconnect",             .callback = disconnect,             .auth = NULL,
                .info = "Disconnect source id and sink id" ,    .session = AFB_SESSION_NONE},
{ .verb = "setVolume",              .callback = setVolume,              .auth = NULL,
                .info = "Set volume value" ,                    .session = AFB_SESSION_NONE}, /* it is better to show the range*/
{ .verb = "volumeStep",             .callback = volumeStep,             .auth = NULL,
                .info = "Set volume step range" ,               .session = AFB_SESSION_NONE},
{ .verb = "setSinkMuteState",       .callback = setSinkMuteState,       .auth = NULL,
                .info = "Set Mute state: 1 means mute, 2 means umute. Others are invalid" ,.session = AFB_SESSION_NONE},
{ .verb = "getListMainConnections", .callback = getListMainConnections, .auth = NULL,
                .info = "Get MainConnection List" ,             .session = AFB_SESSION_NONE},
{ .verb = "getListMainSinks",       .callback = getListMainSinks,       .auth = NULL,
                .info = "Get MainSink List" ,                   .session = AFB_SESSION_NONE},
{ .verb = "getListMainSources",       .callback = getListMainSources,       .auth = NULL,
                .info = "Get MainSource List" ,                 .session = AFB_SESSION_NONE},
{ .verb = "registerSource",         .callback = registerSource,         .auth = NULL,
                .info = "Register autio role" ,                 .session = AFB_SESSION_NONE},
{ .verb = "deregisterSource",       .callback = deregisterSource,       .auth = NULL,
                .info = "Deregister audio role" ,               .session = AFB_SESSION_NONE},
{ .verb = "ackConnect",             .callback = ackConnect,             .auth = NULL,
                .info = "Acknowledge of asyncConnect" ,         .session = AFB_SESSION_NONE},
{ .verb = "ackDisconnect",          .callback = ackDisconnect,          .auth = NULL,
                .info = "Acknowledge of asyncConnect" ,         .session = AFB_SESSION_NONE},
{ .verb = "ackSetSourceState",      .callback = ackSetSourceState,      .auth = NULL,
                .info = "Acknowledge of asyncSetSourceState" ,  .session = AFB_SESSION_NONE},
{ .verb = "subscribe",              .callback = subscribe,              .auth = NULL,
                .info = "Subscribe event" ,                     .session = AFB_SESSION_NONE},
{ .verb = "unsubscribe",            .callback = unsubscribe,            .auth = NULL,
                .info = "Unsubscribe event" ,                   .session = AFB_SESSION_NONE},
{ .verb = NULL } /* marker for end of the array */};

int preinit();
int sm_init();
void onevent(const char *event, struct json_object *object);

const struct afb_binding_v2 afbBindingV2 = {
    .api = "soundmanager",
    .specification = NULL,
    .verbs = binding_verbs,
    .preinit = preinit,
    .init = sm_init,
    .onevent = onevent
};

#endif //SOUNDMANAGER_H