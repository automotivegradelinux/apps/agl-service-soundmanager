/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sm-error.h"
#include "sm-def.h"

const char* get_response_audiomanager_massage_error(int am_error_code)
{
    switch (am_error_code){
        case UNABLE_SEND:
            return "Unable to send request to audiomanager";
        case NOT_INITIALIZED:
            return "soundmanager is not initialized";
        case OK:
            return "OK";
        case UNKNOWN:
            return "unknown error";
        case OUT_RANGE:
            return "value out of range";
        case NOT_USED:
            return "not used";
        case DATABASE_ERR:
            return "database error occured";
        case OBJECT_ALREADY_EXIST:
            return "the desired object already exists";
        case NO_CHANGE:
            return "there is no change";
        case ACTION_IMPOSSIBLE:
            return "the desired action is not possible";
        case OBJECT_NOT_EXIST:
            return "the desired object is non existent";
        case ASYNC_ACTION_ABORTED:
            return "the asynchronous action was aborted";
        case CONNECTION_FORMAT_ERR:
            return "connectionFormat is not selected";
        case COMMUNICATION_ERR:
            return "communication error";
        case EVENT_NOT_EXIST:
            return "desired event doesn't exist";
        default:
            return "Audio Manager responsed unknown error number";
    }
}

const char* get_source_state_key(int am_source_state){
    switch (am_source_state){
        case 0:
            return "unknown";
        case 1:
            return "on";
        case 2:
            return "off";
        case 3:
            return "paused";
        default:
            return "";
    }
}