/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdlib.h>
#include "sm-pending.h"

struct pending* add_pending(struct pending *list, int source_id)
{
    struct pending* pd;

    if(list == NULL){
        pd = malloc(sizeof(struct pending));
        if(NULL == pd){
            AFB_WARNING("memory allocation error");
            return NULL;
        }
        pd->source.sourceID = source_id;
        pd->next = NULL;
        return pd;
    }
    else{
        list->next = add_pending(list->next, source_id);
        return list;
    }
}

struct pending* get_pending(struct pending *list, int source_id){
    if(list == NULL){
        return NULL;
    }
    if(list->source.sourceID == source_id){
        return list;
    }
    else{
        struct pending* pd = get_pending(list->next, source_id);
        return pd;
    }
}

struct pending* del_pending(struct pending* list, int source_id){
    struct pending* tmp;
    if(list == NULL){
        return NULL;
    }

    if(list->source.sourceID == source_id){
        tmp = list->next;
        free(list);
        return tmp;
    }
    else{
        list->next = del_pending(list->next, source_id);
        return list;
    }
}

void del_all_pendings(struct pending *list){
    struct pending* tmp;
    if(list != NULL){
        tmp = list->next;
        free(list);
        del_all_pendings(tmp);
    }
}