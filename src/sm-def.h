/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SOUNDMANAGER_DEFINE_H
#define SOUNDMANAGER_DEFINE_H

#define AM_NAME "org.genivi.audiomanager"
#define AM_CMD_PATH     "/org/genivi/audiomanager/commandinterface"
#define AM_ROUTE_PATH   "/org/genivi/audiomanager/routinginterface"
#define SOUND_MANAGER_RETURN_INTERFACE   "org.genivi.audiomanager.routing.soundmanager"
#define SOUND_MANAGER_BUS_NAME "org.genivi.audiomanager.routing.soundmanager"
#define SOUND_MANAGER_PATH "/org/genivi/audiomanager/routing/soundmanager"

#define COMMAND_EVENT_NUM 10
#define ROUTING_EVENT_NUM 10
#define DEFAULT_SINK 1
#define DEFAULT_SOURCE_CLASS_ID 101
#define DYNAMIC_DOMAIN_ID 100
#define DEFAULT_DOMAIN_ID 0
#define DYNAMIC_SOURCE_ID 0
#define DEFAULT_VOLUME 100
#define DEFAULT_AVAILABLES 1
#define DEFAULT_CONNECTION_FORMAT 2
#define DEFAULT_INTERRUPT 0
#define DEFAULT_SOURCE_STATE 2
#define DS_CONTROLLED 1

#define EVENT_SUBSCRIBE_ERROR_CODE 100

#define KEY_SOURCE_ID "sourceID"
#define KEY_SINK_ID "sinkID"
#define KEY_SINK_NAME "sinkName"
#define KEY_MAIN_CONNECTION_ID "mainConnectionID"
#define KEY_DELAY "delay"
#define KEY_CONNECTION_STATE "connectionState"
#define KEY_CONNECTION_ID "connectionID"
#define KEY_VOLUME "volume"
#define KEY_VOLUME_STEP "volumeStep"
#define KEY_INTERRUPT "interrupt"
#define KEY_MUTE_STATE "muteState"

#define KEY_DOMAIN_ID "domainID"
#define KEY_HANDLE "handle"
#define KEY_APPNAME "audio_role"
#define KEY_RAMP "ramp"
#define KEY_TIME "time"
#define KEY_SOURCE_STATE "sourceState"
#define KEY_SOURCE_CLASS_ID "sourceClassID"
#define KEY_SINK_CLASS_ID "sinkClassID"
#define KEY_ERROR "error"
#define KEY_SINK_DATA "sinkData"
#define KEY_SOURCE_DATA "sourceData"
#define KEY_INTERRUPT_STATE "interruptState"
#define KEY_AVAILABILITY "availability"
#define KEY_AVAILABILITY_REASON "availabilityReason"
#define KEY_LIST_VOLUMES "listVolumes"
#define KEY_PAYLOAD "payload"
#define KEY_CONNECTION_FORMAT "connectionFormat"
#define KEY_EVENT "event"

#define KEY_RESPONSE "response"

#define SM_EVENT_VOLUME_CHANGED                "volumeChanged"
#define SM_EVENT_NEW_MAIN_CONNECTION           "newMainConnection"
#define SM_EVENT_REMOVED_MAIN_CONNECTION       "removedMainConnection"
#define SM_EVENT_SINK_MUTE_STATE_CHANGED       "sinkMuteStateChanged"
#define SM_EVENT_MAIN_CONNECTION_STATE_CHANGED "mainConnectionStateChanged"
/* Routing event*/
#define SM_EVENT_SET_ROUTING_READY             "setRoutingReady"
#define SM_EVENT_SET_ROUTING_RUNDOWN           "setRoutingRundown"
#define SM_EVENT_ASYNC_CONNECT                 "asyncConnect"
#define SM_EVENT_ASYNC_SET_SOURCE_STATE        "asyncSetSourceState"
#define SM_EVENT_ASYNC_DISCONNECT              "asyncDisconnect"
#define SM_EVENT_STREAM_STATE_EVENT            "stream_state_event"

#ifdef  ENABLE_AGL_AHL
#define KEY_AHL_AUDIO_ROLE "audio_role"
#define KEY_AHL_ENDPOINT_ID "endpoint_id"
#define KEY_AHL_ENDPOINT_TYPE "endpoint_type"
#define KEY_AHL_REP_STREAM_ID "stream_id"
#define KEY_AHL_EVENT_NAME "event_name"
#define KEY_AHL_STATE_EVENT "state_event"
#define AHL_EVENT_NAME "ahl_stream_state_event"
#define KEY_AHL_MUTE "mute"
#define AHL_STREAM_UNMUTE 0
#define AHL_STREAM_MUTE 1

typedef enum {
  ENDPOINT_SINK,
  ENDPOINT_SOURCE,
} EndPointType;
#endif

typedef enum {
  NOT_INITIALIZED = -2,
  UNABLE_SEND,
  OK = 0,
  UNKNOWN,
  OUT_RANGE,
  NOT_USED,
  DATABASE_ERR,
  OBJECT_ALREADY_EXIST,
  NO_CHANGE,
  ACTION_IMPOSSIBLE,
  OBJECT_NOT_EXIST,
  ASYNC_ACTION_ABORTED,
  CONNECTION_FORMAT_ERR,
  COMMUNICATION_ERR,
  EVENT_NOT_EXIST = 100
} ErrorCode;

typedef enum {
    REQ_FAIL,
    OUT_OF_RANGE,
    NOT_NUMBER,
    REQ_OK
} REQ_ERROR;

#define MAX_LENGTH_STR 256

#endif // SOUNDMANAGER_DEFINE_H