/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <json-c/json.h>
#include <stdarg.h>
#include "sm-helper.h"
#include "sm-error.h"

static int default_sinkID = DEFAULT_SINK;

REQ_ERROR get_value_uint16(const struct afb_req request, const char *source, uint16_t *out_id)
{
    char* endptr;
    const char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > UINT16_MAX) || (tmp_id < 0) )
    {
        return OUT_OF_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (uint16_t)tmp_id;
    return REQ_OK;
}

REQ_ERROR get_value_int16(const struct afb_req request, const char *source, int16_t *out_id)
{
    char* endptr;
    const char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > INT16_MAX) || (tmp_id < INT16_MIN) )
    {
        return OUT_OF_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (int16_t)tmp_id;
    return REQ_OK;
}

REQ_ERROR get_value_int32(const struct afb_req request, const char *source, int32_t *out_id)
{
    char* endptr;
    const char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > INT32_MAX) || (tmp_id < INT32_MIN) )
    {
        return OUT_OF_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (int32_t)tmp_id;
    return REQ_OK;
}

REQ_ERROR get_sink_id(const afb_req request, const char* key, uint16_t* out_sink_id){
    AFB_DEBUG("");
    REQ_ERROR result = REQ_FAIL;
    const char* requested_sink_str = afb_req_value (request, key);
    json_object* test = afb_req_json(request);
    AFB_DEBUG("%s", json_object_to_json_string_ext(test,  JSON_C_TO_STRING_PRETTY));

    if(0 == strcmp(requested_sink_str, "default")){
        *out_sink_id = default_sinkID;
        AFB_INFO("sinkID(endpointID) is attached as default sinkID as %d", DEFAULT_SINK);
        result = REQ_OK;
    }
    else{
        result = get_value_uint16(request, key, out_sink_id);
        AFB_INFO("sinkID(endpointID) is %d", *out_sink_id);
    }

    if(REQ_OK != result){
        AFB_INFO("can't parse %s, result %d", key, result);
        afb_req_fail_f(request,"wrong-request","can't parse %s, result: %d", key, result);
        return result;
    }
    return REQ_OK;
}

void set_default_sinkID(int sinkID){
    default_sinkID = sinkID;
}

void sm_add_object_to_json_object(struct json_object* j_obj, int count,...)
{
    va_list args;
    va_start(args, count);
    for(int i = 0; i < count; ++i )
    {
        char *key = va_arg(args, char*);
        int value = va_arg(args, int);
        json_object_object_add(j_obj, key, json_object_new_int((int32_t)value));
        ++i;
    }
    va_end(args);
}

void sm_add_object_to_json_object_func(struct json_object* j_obj, const char* verb_name, int count, ...)
{
    va_list args;
    va_start(args, count);

    json_object_object_add(j_obj,"verb", json_object_new_string(verb_name));

    for(int i = 0; i < count; ++i )
    {
        char *key = va_arg(args, char*);
        int value = va_arg(args, int);
        json_object_object_add(j_obj, key, json_object_new_int((int32_t)value));
        ++i;
    }
    va_end(args);
}

int sm_search_event_name_index(const char* value)
{
    size_t buf_size = 50;
    size_t size = sizeof cmd_evlist / sizeof *cmd_evlist;
    int ret = -1;
    for(size_t i = 0 ; i < size ; ++i)
    {
        if(!strncmp(value, cmd_evlist[i], buf_size))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

int sm_search_routing_event_name_index(const char* value)
{
        size_t buf_size = 50;
    size_t size = sizeof route_evlist / sizeof *route_evlist;
    int ret = -1;
    for(size_t i = 0 ; i < size ; ++i)
    {
        if(!strncmp(value, route_evlist[i], buf_size))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

bool send_result(ErrorCode ec, struct afb_req req, const char* function){
    /* GError = NULL means success in case of gdbus request */
    if(ec == UNABLE_SEND)
    {
        AFB_ERROR("Unable to call : %s", function);
        afb_req_fail_f(req, "failed", "%s : %s", function,
            get_response_audiomanager_massage_error(ec));
        return false;
    }
    return true;
}

bool send_result_no_resp(ErrorCode ec, const char* function){
    if(ec == UNABLE_SEND){
        AFB_ERROR("Unable to call : %s", function);
        return false;
    }
    return true;
}

ErrorCode is_range_over_guint16(int source){
    /* error check of range */
    if( (source > UINT16_MAX) || (source < 0) )
    {
        return OUT_RANGE;
    }
    return OK;
}

ErrorCode is_range_over_gint16(int source){
    /* error check of range */
    if( (source > INT16_MAX) || (source < INT16_MIN) )
    {
        return OUT_RANGE;
    }
    return OK;
}