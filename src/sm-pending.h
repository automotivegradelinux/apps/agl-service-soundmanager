/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SOUNDMANAGER_PENDING_H
#define SOUNDMANAGER_PENDING_H

#define _GNU_SOURCE
#define AFB_BINDING_VERSION 2
#include <afb/afb-binding.h>

struct pending_source{
    int sourceID;
};
struct pending{
    struct pending_source source;
    struct pending* next;
};


struct pending* add_pending(struct pending *list, int source_id);
struct pending* get_pending(struct pending *list, int source_id);
struct pending* del_pending(struct pending* list, int source_id);
void del_all_pendings(struct pending *list);

#endif //SOUNDMANAGER_PENDING_H