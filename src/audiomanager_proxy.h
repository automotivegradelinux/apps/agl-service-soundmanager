/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUDIO_MANAGER_PROXY_H
#define AUDIO_MANAGER_PROXY_H
#include <glib.h>
#include <stdbool.h>
#include "sm-def.h"

typedef struct audiomanager_event{
    void (*on_new_main_connection)(int connection, int source, int sink, int delay, int connection_state);
    void (*on_removed_main_connection)(int connection);
    void (*on_main_connection_state_changed)(int connection, int connection_state);
    void (*on_volume_changed)(int sink, int volume);
    void (*on_sink_mute_state_changed)(int sink, int mute);
    void (*on_set_routing_ready)(void);
    void (*on_set_routing_rundown)(void);
} am_event;

typedef struct audiomanager_instruction{
    void (*on_async_abort)(int handle);
    void (*on_async_connect)(int handle, int connection, int source, int sink, int connection_format);
    void (*on_async_disconnect)(int handle, int connection);
    void (*on_async_set_sink_volume)(int handle, int sink, int volume, int ramp, int time);
    void (*on_async_set_source_state)(int handle, int source, int source_state);
} am_instruction;

struct domain_data{
    int     domainID;
    char*   name;
    char*   busname;
    char*   nodename;
    bool    early;
    bool    complete;
    int     state;
};

struct sound_property_s{
    int type;
    int value;
};
struct availability_s{
    int availability;
    int avalilable_reason;
};
struct notification_config_s{
    int type;
    int status;
    int parameter;
};
struct main_sound_property_s{
    int type;    /* am_CustomMainSoundPropertyType_t */
    int value;
};

ErrorCode am_proxy_connect(int source, int sink, int *main_connection_id);
ErrorCode am_proxy_disconnect(int main_connection_id);
ErrorCode am_proxy_set_volume(int sink, int volume);
ErrorCode am_proxy_volume_step(int sink, int volume);
ErrorCode am_proxy_set_sink_mute_state(int sink, int mute_state);
ErrorCode am_proxy_get_list_main_connections(GVariant** connection_list);
ErrorCode am_proxy_get_list_main_sources(GVariant** source_list);
ErrorCode am_proxy_get_list_main_sinks(GVariant** sink_list);
ErrorCode am_proxy_ack_connect(int handle, int connection_id, int usr_err);
ErrorCode am_proxy_ack_disconnect(int handle, int connection_id, int usr_err);
ErrorCode am_proxy_ack_set_source_state(int handle, int usr_err);
ErrorCode am_proxy_register_source(GVariant *source_data, int *source);
ErrorCode am_proxy_deregister_source(int source);
ErrorCode am_proxy_register_domain(GVariant* domain_data, int *domain);

GVariant* create_domain_data(struct domain_data* domain);
GVariant* create_source_data(int sourceID, int domainID, const char* appname, int sourceClassID,
    int  sourceState, int volume, bool visible, struct availability_s availables,
    int interrupt,  struct sound_property_s soundPropertyList, int connectionFormatList,
    struct main_sound_property_s mainPropertyList, struct notification_config_s NConfRouting,
    struct notification_config_s NConfCommand);

ErrorCode initialize_proxy();
void set_event_callback(const am_event *callback);
void close_proxy();
ErrorCode open_soundmanager_interface(const am_instruction *callback);
void close_soundmanager_inerface();

#endif //AUDIO_MANAGER_PROXY_H