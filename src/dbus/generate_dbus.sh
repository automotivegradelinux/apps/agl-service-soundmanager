#
# Copyright (c) 2017 TOYOTA MOTOR CORPORATION
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#!/bin/sh
echo "generate dbus c code"
gdbus-codegen \
    --interface-prefix org.genivi. \
    --generate-c-code audio_manager_interface \
    --c-generate-object-manager \
    command_interface.xml routing_interface.xml sound_manager_interface.xml

echo "end generate dbus c code"
