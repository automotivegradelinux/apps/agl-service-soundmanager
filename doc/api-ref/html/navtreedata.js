var NAVTREE =
[
  [ "Sound Manager", "index.html", [
    [ "**Sound Manager Application Guide**", "md__r_1__sound_manager_binding_doc__application_guide.html", null ],
    [ "Sound mode transition for single window application", "md__r_1__sound_manager_binding_doc__display__audio__transition1.html", null ],
    [ "Sound mode transition for dual window application", "md__r_1__sound_manager_binding_doc__display__audio__transition2.html", null ],
    [ "This is a SoundManager implementation for the AGL Project.", "md__r_1__sound_manager_binding__r_e_a_d_m_e.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"audio__manager__interface_8h.html#a3c9f64cfa0a7b51bb5629b39eb19628d",
"dir_dc6b799b4d4b6fe07d59a0bf0ebf173c.html",
"structevent.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';