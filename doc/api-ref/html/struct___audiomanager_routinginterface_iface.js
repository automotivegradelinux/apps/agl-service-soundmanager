var struct___audiomanager_routinginterface_iface =
[
    [ "handle_ack_connect", "struct___audiomanager_routinginterface_iface.html#ae94f0c43f9dcb471040598f63f696a7a", null ],
    [ "handle_ack_disconnect", "struct___audiomanager_routinginterface_iface.html#ad19ecd30baab6e7d4ba6d6c6002cfaf9", null ],
    [ "handle_ack_set_sink_volume", "struct___audiomanager_routinginterface_iface.html#ab6e540b11ad03b27b947b79f268ce1a9", null ],
    [ "handle_ack_set_source_state", "struct___audiomanager_routinginterface_iface.html#aa08081f0b75bceb2d649b993e64780e0", null ],
    [ "handle_ack_set_volumes", "struct___audiomanager_routinginterface_iface.html#ae8345befe28426327325b1ba4df642ee", null ],
    [ "handle_ack_sink_notification_configuration", "struct___audiomanager_routinginterface_iface.html#ab6dead68bb1d960873c1d3b43b16ed75", null ],
    [ "handle_ack_source_notification_configuration", "struct___audiomanager_routinginterface_iface.html#a29c4ad940905f828e5a8e5ac0433a3ae", null ],
    [ "handle_confirm_routing_ready", "struct___audiomanager_routinginterface_iface.html#ab247488d2597997aca94ab969f8d3591", null ],
    [ "handle_confirm_routing_rundown", "struct___audiomanager_routinginterface_iface.html#ad105905907e3735fe601fd472b658787", null ],
    [ "handle_deregister_sink", "struct___audiomanager_routinginterface_iface.html#a7a549ccc940b2d7bdfe044bf0080cab5", null ],
    [ "handle_deregister_source", "struct___audiomanager_routinginterface_iface.html#a922e3b64fda18253a4876b08297af520", null ],
    [ "handle_hook_interrupt_status_change", "struct___audiomanager_routinginterface_iface.html#aaa5a962b2eabddde63e3379c86204661", null ],
    [ "handle_hook_source_availablity_status_change", "struct___audiomanager_routinginterface_iface.html#a3e7e0beac27ee45449766260f0f52d2b", null ],
    [ "handle_register_domain", "struct___audiomanager_routinginterface_iface.html#abf7f3af5c44437c7e137d953e212796b", null ],
    [ "handle_register_sink", "struct___audiomanager_routinginterface_iface.html#aae0f751fca4ed3afbe8c6949bbf5b239", null ],
    [ "handle_register_source", "struct___audiomanager_routinginterface_iface.html#ab4d210b0dc6778973ff8494aabd2d73a", null ],
    [ "parent_iface", "struct___audiomanager_routinginterface_iface.html#a2235fa9640d924dfa2810cab5c349812", null ],
    [ "set_routing_ready", "struct___audiomanager_routinginterface_iface.html#a58f92c78dae0850dfdc5b4096957cdfb", null ],
    [ "set_routing_rundown", "struct___audiomanager_routinginterface_iface.html#a2506d6398156498330ecd4beb9f1b44d", null ]
];