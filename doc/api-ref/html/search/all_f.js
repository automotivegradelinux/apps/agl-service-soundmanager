var searchData=
[
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['register_5fcallback',['register_callback',['../class_lib_soundmanager.html#a472149619f68c2387d4ff7e02845db26',1,'LibSoundmanager::register_callback(void(*event_cb)(const std::string &amp;event, struct json_object *event_contents), void(*reply_cb)(struct json_object *reply_contents), void(*hangup_cb)(void)=nullptr)'],['../class_lib_soundmanager.html#ab7fd9ce3fae6ab6e0806ff099ad681f0',1,'LibSoundmanager::register_callback(void(*reply_cb)(struct json_object *reply_contents), void(*hangup_cb)(void)=nullptr)']]],
  ['registersource',['registerSource',['../class_lib_soundmanager.html#a5353df3686a64c74c9672efd0c156351',1,'LibSoundmanager']]],
  ['removed_5fmain_5fconnection',['removed_main_connection',['../struct___audiomanager_commandinterface_iface.html#a79a550450c8e7796acae307e5562142e',1,'_AudiomanagerCommandinterfaceIface']]],
  ['req_5ferror',['REQ_ERROR',['../sm-helper_8h.html#aa49f1dbbf26f01627a5737cf43aad899',1,'REQ_ERROR():&#160;sm-helper.h'],['../sm-helper_8h.html#ab0d62ccfa9c3ab87f090f67c3d50adce',1,'REQ_ERROR():&#160;sm-helper.h']]],
  ['req_5ffail',['REQ_FAIL',['../sm-helper_8h.html#aa49f1dbbf26f01627a5737cf43aad899a96a855966bc63045222b3dcac524cee1',1,'sm-helper.h']]],
  ['req_5fok',['REQ_OK',['../sm-helper_8h.html#aa49f1dbbf26f01627a5737cf43aad899ab093abb14c097b3b7719debb04d5e8ee',1,'sm-helper.h']]],
  ['routing_5fevent_5fnum',['ROUTING_EVENT_NUM',['../soundmanager_8c.html#a33d1c40de8a5e7a3d6f0e2f45de9f37f',1,'soundmanager.c']]]
];
