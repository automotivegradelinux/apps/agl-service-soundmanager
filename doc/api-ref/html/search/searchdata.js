var indexSectionsWithContent =
{
  0: "_abcdeghilmnopqrstuv~",
  1: "_acdelmns",
  2: "adlrs",
  3: "acdgilorsu~",
  4: "abcdehilmnopqrstuv",
  5: "ahor",
  6: "er",
  7: "enor",
  8: "_acdeiorst",
  9: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

