var struct___audiomanager_routing_soundmanager_iface =
[
    [ "handle_async_abort", "struct___audiomanager_routing_soundmanager_iface.html#a2533187c934a62aa925bc33f36e0842d", null ],
    [ "handle_async_connect", "struct___audiomanager_routing_soundmanager_iface.html#aaf21bf6e7f0a53cc0ff682ab84915916", null ],
    [ "handle_async_disconnect", "struct___audiomanager_routing_soundmanager_iface.html#a4afe4885e0f8effeec506c7c248ce406", null ],
    [ "handle_async_set_sink_volume", "struct___audiomanager_routing_soundmanager_iface.html#ad50e3c8bf63145db7b2793f3f09a8a1a", null ],
    [ "handle_async_set_source_state", "struct___audiomanager_routing_soundmanager_iface.html#afa12d6ca18a68e5ef319b6f5d4830ad3", null ],
    [ "parent_iface", "struct___audiomanager_routing_soundmanager_iface.html#a4bb6a0f7d7b7df2c18a9b54f9023d308", null ]
];