var struct___audiomanager_commandinterface_iface =
[
    [ "handle_connect", "struct___audiomanager_commandinterface_iface.html#a43990993bb9ea56972ccc7fd111ca1da", null ],
    [ "handle_disconnect", "struct___audiomanager_commandinterface_iface.html#aa109962beceb7ee6063060e9d5031a12", null ],
    [ "handle_get_list_main_connections", "struct___audiomanager_commandinterface_iface.html#aa446e43ef09c4354da0111d9b29ddc61", null ],
    [ "handle_get_list_main_sinks", "struct___audiomanager_commandinterface_iface.html#a1367ae3bc1e66ce85f4e99978225304a", null ],
    [ "handle_set_sink_mute_state", "struct___audiomanager_commandinterface_iface.html#a02d3f2d14c1799b074ec446492e62539", null ],
    [ "handle_set_volume", "struct___audiomanager_commandinterface_iface.html#a6ebd74e48b92340de3239fe898ecadbc", null ],
    [ "handle_volume_step", "struct___audiomanager_commandinterface_iface.html#a196093f8f4e65d4b3aecd0a7e5bcdada", null ],
    [ "main_connection_state_changed", "struct___audiomanager_commandinterface_iface.html#a60c7be0e93dcdeebc02e3ec6e5597545", null ],
    [ "new_main_connection", "struct___audiomanager_commandinterface_iface.html#acf8ac7ac76b76708883e08af86c87116", null ],
    [ "parent_iface", "struct___audiomanager_commandinterface_iface.html#a551ebf6d7c02f5efa18eb2ec170601e4", null ],
    [ "removed_main_connection", "struct___audiomanager_commandinterface_iface.html#a79a550450c8e7796acae307e5562142e", null ],
    [ "sink_mute_state_changed", "struct___audiomanager_commandinterface_iface.html#add4f7a9df53cd7dfa4ae5286a6822b9f", null ],
    [ "system_property_changed", "struct___audiomanager_commandinterface_iface.html#a22c033bae26a6343ec05890b815bd238", null ],
    [ "volume_changed", "struct___audiomanager_commandinterface_iface.html#a12178f8ea420184dc12314a6b297f044", null ]
];